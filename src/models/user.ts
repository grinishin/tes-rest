import {Document, Schema, model} from 'mongoose';
import * as bcrypt from 'bcryptjs';

export interface IUser extends Document {
  name: string;
  username: string;
  email: string;
  password: string;

  comparePassword(candidatePassword: string): Promise<boolean>;
}

export const schema = new Schema({
  name:     String,
  email: {
    type:     String,
    required: true,
    unique:   true
  },
  username: {
    type:     String,
    unique:   true
  },
  password: {
    type:     String,
    required: true
  }
}, {timestamps: {createdAt: 'created_at', updatedAt: 'updated_at'}});

schema.pre('save', (next) => {
  bcrypt.hash(this.password, 10, (err, hash) => {
    this.password = hash;
    next();
  });
});

schema.pre('update', (next) => {
  bcrypt.hash(this.password, 10, (err, hash) => {
    this.password = hash;
    next();
  });
});

schema.methods.comparePassword = (candidatePassword: string) => {
  const password = this.password;
  return new Promise((resolve, reject) => {
    bcrypt.compare(candidatePassword, password, (err, success) => {
      if (err) {
        return reject(err);
      }
      return resolve(success);
    });
  });
};

export const userModel = model<IUser>('User', schema);

export const cleanCollection = () => userModel.remove({}).exec();

export default userModel;
