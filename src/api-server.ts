import * as express from 'express';
import {Server} from 'typescript-rest';
import * as http from 'http';
import * as path from 'path';
import * as mongoose from 'mongoose';
import * as cors from 'cors';
import * as bodyParser from 'body-parser';
import * as expressValidator from 'express-validator';
import controllers from './controllers';

export class ApiServer {

  private readonly app: express.Application;
  static readonly localPort: null = 3350;
  private server: http.Server = null;
  public PORT: number = +process.env.PORT || ApiServer.localPort;

  constructor() {
    this.app = express();
    this.config();

    Server.useIoC();
    Server.buildServices(this.app, ...controllers);

    // TODO: enable for Swagger generation error
    // Server.loadServices(this.app, 'controllers/*', __dirname);
    Server.swagger(this.app, './dist/swagger.json', '/api-docs', `localhost:${ApiServer.localPort}`, ['http']);
  }

  /**
   * Configure the express app.
   */
  private config(): void {
    const MONGO_URI: string = 'mongodb://localhost/tes';
    mongoose.connect(MONGO_URI || process.env.MONGODB_URI, {useNewUrlParser: true});


    this.app.use((req, res, next) => {
      res.header('Access-Control-Allow-Origin', 'http://localhost:8080');
      res.header(
        'Access-Control-Allow-Methods',
        'GET, POST, PUT, DELETE, OPTIONS',
      );
      res.header(
        'Access-Control-Allow-Headers',
        'Origin, X-Requested-With, Content-Type, Accept, Authorization, Access-Control-Allow-Credentials',
      );
      res.header('Access-Control-Allow-Credentials', 'true');
      next();
    });

    this.app.use(expressValidator());
    this.app.use(express.static(path.join(__dirname, 'public'), {maxAge: 31557600000}));
    this.app.use(cors());
  }

  /**
   * Start the server
   * @returns {Promise<any>}
   */
  public start(): Promise<any> {
    return new Promise<any>((resolve, reject) => {
      this.server = this.app.listen(this.PORT, (err: any) => {
        if (err) {
          return reject(err);
        }

        console.log(this.server.address());
        console.log(`Listening to http://${this.server.address().address}:${this.server.address().port}`);

        return resolve();
      });
    });

  }

  /**
   * Stop the server (if running).
   * @returns {Promise<boolean>}
   */
  public stop(): Promise<boolean> {
    return new Promise<boolean>((resolve, reject) => {
      if (this.server) {
        this.server.close(() => {
          return resolve(true);
        });
      } else {
        return resolve(true);
      }
    });
  }

}
