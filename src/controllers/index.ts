import { HelloIocDirectController, HelloIocInterfaceController } from './hello-ioc-controller';
import { HelloServiceBase, HelloServiceImpl, IocHelloService } from './ioc-services';
import {AuthController} from './auth/auth.controller';
import {UserController} from './user/user.controller';

export default [

  // The IOC controllers
  HelloIocDirectController,
  HelloIocInterfaceController,

  // Don't forget to load these services, or IOC won't find them.
  IocHelloService,
  HelloServiceBase,
  HelloServiceImpl,

  AuthController,
  UserController
];
