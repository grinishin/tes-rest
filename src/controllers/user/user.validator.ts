import {Request} from 'express';
import {Errors} from 'typescript-rest';
import {throws} from 'assert';

export class UserValidator {

  onCreateOrUpdate(request: Request, update?: boolean): Promise<any> {
    request.checkBody('email', 'The email cannot be empty').notEmpty();
    request.checkBody('email', 'Wrong email format').isEmail();
    request.checkBody('password', 'The password cannot be empty').notEmpty();

    const errors: any = request.validationErrors();

    return new Promise((resolve) => {
      if (errors) {
        throw new Errors.BadRequestError(errors.map(({msg}) => msg).join('\n'))
      }
      resolve();
    });


  }
}
