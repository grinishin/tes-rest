import {IUser, userModel as User} from '../../models/user';
import {Inject} from 'typescript-ioc';
import {ContextRequest, ContextResponse, Path, POST} from 'typescript-rest';
import {UserValidator} from './user.validator';
import {Request, Response} from 'express';
import {Errors} from "typescript-rest";

@Path('/user')
export class UserController {

  @Inject private userValidator: UserValidator;

  @POST
  async create(user: IUser, @ContextRequest request: Request, @ContextResponse res: Response) {

    return await this.userValidator.onCreateOrUpdate(request).then(
      () => {
        return new Promise((resolveValidated) => {
          const equivName = user.email.split('@')[0];

          const newUser = new User({
            name:     user.name || equivName,
            username: user.username || equivName,
            email:    user.email,
            password: user.password
          });

          resolveValidated(newUser.save()
            .catch(err => {
              console.log(err.errmsg);
              throw new Errors.ConflictError(err.errmsg);
            })
            .then(saved => saved));

        });
      }
    )
  }
}
